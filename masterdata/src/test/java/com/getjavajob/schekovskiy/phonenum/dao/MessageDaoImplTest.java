package com.getjavajob.schekovskiy.phonenum.dao;

import com.getjavajob.schekovskiy.phonenum.model.Message;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/test-config.xml"})
public class MessageDaoImplTest {

    @Autowired
    private MessageDao dao;

    @After
    public void tearDown() {
        dao.clear();
    }

    @Test
    public void testAddAndGetByName() throws Exception {
        Message msg = new Message("TestName1", "Message_1");
        dao.add(msg);
        List<Message> msgList = dao.getByName("TestName1");
        assertFalse(msgList.isEmpty());
    }

    @Test
    public void testGet() throws Exception {
        String testName = "Vasya";
        Message msg1 = new Message(testName, "Message_1");
        Message msg2 = new Message(testName, "Message_2");
        Message msg3 = new Message(testName, "Message_3");

        dao.add(msg1);
        List<Message> msgList = dao.getByName(testName);
        Date record_time1 = msgList.get(0).getRecordTime();
        Thread.sleep(2000);
        dao.add(msg2);
        msgList = dao.getByName(testName);
        Date record_time2 = msgList.get(1).getRecordTime();
        Thread.sleep(2000);
        dao.add(msg3);
        msgList = dao.getByName(testName);
        Date record_time3 = msgList.get(2).getRecordTime();

        msgList = dao.get(testName, new Date(1L), new Date());
        assertEquals(record_time1, msgList.get(0).getRecordTime());
        assertEquals(record_time2, msgList.get(1).getRecordTime());
        assertEquals(record_time3, msgList.get(2).getRecordTime());

        msgList = dao.get(testName, record_time2, new Date());
        assertEquals(record_time2, msgList.get(0).getRecordTime());
        assertEquals(record_time3, msgList.get(1).getRecordTime());
    }
}
