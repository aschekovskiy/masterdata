package com.getjavajob.schekovskiy.phonenum.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.TimeZone;

public class SearchFilter {

    final static Logger logger = Logger.getLogger(SearchFilter.class);

    private String name;
    @JsonFormat(shape=JsonFormat.Shape.STRING, timezone="GMT+4", pattern="yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    @JsonFormat(shape=JsonFormat.Shape.STRING, timezone = "GMT+4", pattern="yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    public SearchFilter() {
    }

    public SearchFilter(String name, Date startTime, Date endTime) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
