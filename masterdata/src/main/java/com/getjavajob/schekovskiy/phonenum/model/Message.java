package com.getjavajob.schekovskiy.phonenum.model;

import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Message {

    private Long id;
    private String name;
    private String message;
    private Date recordTime;


    public Message() {
    }

    public Message(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public Message(Long id, String name, String message, Date recordTime) {
        this.id = id;
        this.name = name;
        this.message = message;
        this.recordTime = recordTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    @Override
    public String toString() {
        return "PhoneNumber{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", message=" + message +
                ", recordTime='" + recordTime + '\'' +
                '}';
    }
}
