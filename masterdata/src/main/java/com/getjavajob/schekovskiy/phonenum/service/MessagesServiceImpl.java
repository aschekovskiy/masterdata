package com.getjavajob.schekovskiy.phonenum.service;

import com.getjavajob.schekovskiy.phonenum.dao.MessageDao;
import com.getjavajob.schekovskiy.phonenum.model.Message;
import com.getjavajob.schekovskiy.phonenum.model.SearchFilter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MessagesServiceImpl implements MessagesService {

    private final static Logger logger = Logger.getLogger(MessagesServiceImpl.class);

    @Autowired
    MessageDao dao;

    @Override
    public void add(Message msg) {
        dao.add(msg);
    }

    @Override
    public List<Message> get(SearchFilter searchFilter) {
        String name = searchFilter.getName();
        Boolean isHaveTime = false;
        Date startTime = searchFilter.getStartTime();
        Date endTime = searchFilter.getEndTime();
        if (startTime == null) {
            startTime = new Date(1L);
        } else {
            isHaveTime = true;
        }
        if (endTime == null) {
            endTime = new Date();
        } else {
            isHaveTime = true;
        }

        List<Message> msgList;
        if (!name.isEmpty() && isHaveTime) {
            msgList = dao.get(name, startTime, endTime);
        } else if (name.isEmpty() && isHaveTime) {
            msgList = dao.getByTime(startTime, endTime);
        } else {
            msgList = dao.getByName(name);
        }
        logger.info("Search done! [" + msgList.size() + "] records founded for name:" + name + ", startTime:" + startTime + ", endTime:" + endTime);
        return msgList;
    }

    @Override
    public List<Message> getPage() {
        return null;
    }

    @Override
    public void clear() {

    }
}
