package com.getjavajob.schekovskiy.phonenum.service;

import com.getjavajob.schekovskiy.phonenum.model.Message;
import com.getjavajob.schekovskiy.phonenum.model.SearchFilter;

import java.util.List;

public interface MessagesService {

    void add(Message msg);

    List<Message> get(SearchFilter searchFilter);

    List<Message> getPage();

    void clear();

}
