package com.getjavajob.schekovskiy.phonenum.dao;

import com.getjavajob.schekovskiy.phonenum.model.Message;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MessageDaoImpl extends NamedParameterJdbcDaoSupport implements MessageDao {

    private final static Logger logger = Logger.getLogger(MessageDaoImpl.class);
    private final static String pattern = "yyyy-MM-dd HH:mm:ss";
    private final static DateFormat format = new SimpleDateFormat(pattern);
    private static final String ADD_MESSAGE_SQL = "INSERT INTO messages(name,message,record_time) VALUES (:name, :message, :recordTime)";
    private static final String GET_MESSAGE_SQL = "SELECT * FROM messages WHERE name = :name AND " +
            "record_time BETWEEN :startTime AND :endTime";
    private static final String GET_MESSAGE_BY_NAME_SQL = "SELECT * FROM messages WHERE name = :name";
    private static final String GET_MESSAGE_BY_TIME_SQL = "SELECT * FROM messages WHERE record_time BETWEEN :startTime AND :endTime";
    private static final String CLEAR_SQL = "DELETE FROM messages";

    @Override
    public void add(Message msg) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        String message = msg.getMessage();
        param.addValue("name", msg.getName());
        param.addValue("message", message);
        param.addValue("recordTime", format.format(new Date()));
        getNamedParameterJdbcTemplate().update(ADD_MESSAGE_SQL, param);
        logger.info("Message \"" + message + "\" saved!");
    }

    @Override
    public List<Message> get(String name, Date startTime, Date endTime) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("name", name);
        param.addValue("startTime", startTime);
        param.addValue("endTime", endTime);
        return getNamedParameterJdbcTemplate().query(GET_MESSAGE_SQL, param, ROW_MAPPER);
    }

    @Override
    public List<Message> getByName(String name) {
        return getNamedParameterJdbcTemplate().query(GET_MESSAGE_BY_NAME_SQL, new MapSqlParameterSource().addValue("name", name), ROW_MAPPER);
    }

    @Override
    public List<Message> getByTime(Date startTime, Date endTime) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("startTime", startTime);
        param.addValue("endTime", endTime);
        return getNamedParameterJdbcTemplate().query(GET_MESSAGE_BY_TIME_SQL, param, ROW_MAPPER);

    }

    @Override
    public void clear() {
        getJdbcTemplate().execute(CLEAR_SQL);
    }

    private static RowMapper<Message> ROW_MAPPER = new RowMapper<Message>() {
        @Override
        public Message mapRow(ResultSet rs, int rowNum) throws SQLException {
            final Message msg = new Message();
            msg.setId(rs.getLong("id"));
            msg.setName(rs.getString("name"));
            msg.setMessage(rs.getString("message"));
            msg.setRecordTime(rs.getTimestamp("record_time"));
            return msg;
        }
    };
}
