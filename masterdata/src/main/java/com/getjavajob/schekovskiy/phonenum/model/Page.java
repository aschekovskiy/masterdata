package com.getjavajob.schekovskiy.phonenum.model;

import org.apache.log4j.Logger;

import java.util.List;

public class Page<T> {

    private final static Logger logger = Logger.getLogger(Page.class);

    private List<T> records;
    private int size;
    private int totalPages;

    public Page(List<T> records, int currPage, int size) {
        this.size = size;
        int recordSize = records.size();
        totalPages = recordSize / size;
        if (recordSize % size > 0) {
            totalPages++;
        }
        if(currPage <= totalPages) {
            int startIndex = (currPage - 1) * size;
            int endIndex;
            if(currPage == totalPages){
                endIndex = recordSize;
            } else {
                endIndex = startIndex + size;
            }
            this.records = records.subList(startIndex, endIndex);
        } else {
            logger.error("Current records " + currPage +  " can't be more then total pages " + totalPages );
        }
    }

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}