package com.getjavajob.schekovskiy.phonenum.web;

import com.getjavajob.schekovskiy.phonenum.model.Message;
import com.getjavajob.schekovskiy.phonenum.model.Page;
import com.getjavajob.schekovskiy.phonenum.model.SearchFilter;
import com.getjavajob.schekovskiy.phonenum.service.MessagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class PageController {

    private List<Message> messages = new ArrayList<>();
    private final static Integer PAGE_SIZE = 5;
    private final MessagesService messagesService;

    @Autowired
    public PageController(MessagesService messagesService) {
        this.messagesService = messagesService;
    }

    @RequestMapping(value = "messages", method = RequestMethod.GET)
    public String initIndex(Map<String, Object> model) {
        return "messages";
    }

    @RequestMapping(value = "messages", method = RequestMethod.POST)
    public String addMessages(@RequestBody Message msg) {
        messagesService.add(msg);
        return "messages";
    }

    @RequestMapping(value = "search", method = RequestMethod.GET)
    public String initSearch(Map<String, Object> model) {
        return "search";
    }

    @RequestMapping(value = "search", method = RequestMethod.POST)
    public
    @ResponseBody
    Page<Message> search(@RequestBody SearchFilter searchFilter) {
        messages = messagesService.get(searchFilter);
        return new Page<Message>(messages, 1, PAGE_SIZE);
    }

    @RequestMapping(value = "search/getMessagePage", method = RequestMethod.POST)
    public
    @ResponseBody
    Page<Message> getMessagePage(@RequestBody Integer currPage) {
        return new Page<Message>(messages, currPage, PAGE_SIZE);
    }


}
