package com.getjavajob.schekovskiy.phonenum.dao;

import com.getjavajob.schekovskiy.phonenum.model.Message;

import java.util.Date;
import java.util.List;

public interface MessageDao {

    void add(Message msg);

    List<Message> get(String name, Date startDate, Date endDate);

    List<Message> getByName(String name);

    List<Message> getByTime(Date startDate, Date endDate);

    void clear();

}
