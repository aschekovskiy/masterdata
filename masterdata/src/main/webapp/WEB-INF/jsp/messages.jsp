<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="<spring:url value="/resources/scripts/jquery-2.1.1.min.js" htmlEscape="true"/>"></script>
    <link rel="stylesheet" href="<spring:url value="/resources/dist/css/bootstrap.min.css" htmlEscape="true"/>"
          type="text/css"/>
    <link rel="stylesheet" href="<spring:url value="/resources/dist/css/bootstrap-theme.min.css" htmlEscape="true"/>"
          type="text/css"/>
    <link rel="stylesheet" href="<spring:url value="/resources/dist/css/additional.css" htmlEscape="true"/>"
          type="text/css"/>
</head>
<body>
<div class="wrapper">
    <form action="messages" method="post" id="inputNumbersForm">
        <div class="form-group">
            <label for="inputName">Name</label>
            <input type="text" id="inputName">
        </div>
        <div class="form-group">
            <label for="inputMessage" style="vertical-align: top;">Message</label>
            <textarea id="inputMessage"></textarea>
        </div>
        <button type="submit" class="btn btn-default">Send</button>
        <button type="submit" id="searchPage" class="btn btn-default">Search</button>
    </form>
</div>
<script type="text/javascript">
    $(function () {

        $('#searchPage').click(function () {
            window.location.href = '/search';
            return false;
        });

        $('#inputNumbersForm').submit(function (event) {
            event.preventDefault();
            console.log("Message: " + $('#inputMessage').val());
            $.ajax({
                url: '/messages',
                type: "POST",
                data: JSON.stringify({name: $('#inputName').val(), message: $('#inputMessage').val()}),
                contentType: "application/json; charset=utf-8"
            });
        });
    });
</script>
</body>
</html>
