<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<html>
<head>
    <script src="<spring:url value="/resources/scripts/jquery-2.1.1.min.js" htmlEscape="true"/>"></script>
    <script src="<spring:url value="/resources/scripts/jquery.simplePagination.js" htmlEscape="true"/>"></script>
    <link rel="stylesheet" href="<spring:url value="/resources/dist/css/bootstrap.min.css" htmlEscape="true"/>"
          type="text/css"/>
    <link rel="stylesheet" href="<spring:url value="/resources/dist/css/bootstrap-theme.min.css" htmlEscape="true"/>"
          type="text/css"/>
    <link rel="stylesheet" href="<spring:url value="/resources/dist/css/additional.css" htmlEscape="true"/>"
          type="text/css"/>
    <link rel="stylesheet" href="<spring:url value="/resources/dist/css/simplePagination.css" htmlEscape="true"/>"
          type="text/css"/>

</head>
<body>
<div class="wrapper">
    <form action="search" method="post" id="searchForm">
        <div class="form-group">
            <label for="inputName">Name</label>
            <input type="text" class="form-control" id="inputName">
        </div>
        <div class="form-group">
            <label for="inputStartTime">Start time</label>
            <input type="text" class="form-control" id="inputStartTime" placeholder="yyyy-mm-dd hh:mm:ss">
        </div>
        <div class="form-group">
            <label for="inputEndTime">End time</label>
            <input type="text" class="form-control" id="inputEndTime" placeholder="yyyy-mm-dd hh:mm:ss">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
    </form>
    <div id="searchResult"></div>
    <div class="pages" style="margin-top: 20px" id="msgPagintaion"></div>
</div>

<script type="text/javascript">
    $(document).ready(function () {



        $('#searchForm').submit(function (event) {
            event.preventDefault();
            var node = document.getElementById("msgPagintaion");
            node.innerHTML = '';

            $.ajax({
                url: '/search',
                type: "POST",
                data: JSON.stringify({
                    name: $('#inputName').val(), startTime: $('#inputStartTime').val(),
                    endTime: $('#inputEndTime').val()
                }),
                dataType: "json",
                contentType: "application/json; charset=utf-8", success: function (page) {
                    var messages = page.records;
                    var totalPages = parseInt(page.totalPages);
                    console.log("totalPages " + totalPages);
                    console.log("equals " + (totalPages > 1));
                    if (totalPages > 1) {
                        $(".pages").pagination({
                            itemsOnPage: page.size,
                            pages: totalPages,
                            displayedPages: 2,
                            edges: 1,
                            cssStyle: 'compact-theme'
                        });
                    }

                    printPage(messages);
                }
            });
        });

        $('.pages').click(function (e) {
            var currPage = $(this).pagination('getCurrentPage');
            $.ajax({
                url: '/search/getMessagePage',
                type: "POST",
                data: JSON.stringify(currPage),
                dataType: "json",
                contentType: "application/json; charset=utf-8", success: function (page) {
                    printPage(page.records);
                }

            });
        });

        function printPage(msgs) {
            var container = document.getElementById("searchResult");
            container.innerHTML = '';
            for (i = 0, len = msgs.length; i < len; ++i) {
                var msg = document.createElement('div');
                var currMsg = msgs[i];
                var date = new Date(currMsg.recordTime);

                msg.innerHTML = '[' + format(date) + ']' + currMsg.name +
                ': ' + currMsg.message;
                container.appendChild(msg);
            }
        }

        function format(date) {
            var year = date.getYear() + 1900;
            var month = addZero(date.getMonth() + 1);
            var day = addZero(date.getDate());
            var hour = addZero(date.getHours());
            var minutes = addZero(date.getMinutes());
            var seconds = addZero(date.getSeconds());

            return '' + year + '-' + month + '-' + day + ' ' + hour + ':' + minutes
                    + ':' + seconds;

        }

        function addZero(str) {
            if (str < 10) {
                str = '0' + str;
            }
            return str;
        }
    });
</script>
</body>
</html>
