CREATE DATABASE IF NOT EXISTS masterdata;

USE masterdata;

CREATE TABLE IF NOT EXISTS messages (
  id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
  name VARCHAR(30) NOT NULL ,
  message VARCHAR(500) NOT NULL ,
  record_time DATETIME NOT NULL
);
